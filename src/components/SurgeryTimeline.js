import React from "react";
import { extent as d3Extent, max as d3Max } from "d3-array";
import {
  scaleLinear as d3ScaleLinear,
  scaleTime as d3ScaleTime
} from "d3-scale";
import { axisBottom as d3AxisBottom, axisLeft as d3AxisLeft } from "d3-axis";
import { select as d3Select } from "d3-selection";
import { timeMinute as d3TimeMinute } from "d3-time";
import addHours from "date-fns/add_hours";

import "./SurgeryTimeline.css";

const SurgeryTimeline = ({ data, height, width, selectX, selectY }) => {
  const margin = {
    left: 30,
    bottom: 30
  };

  const oneHour = addHours(new Date(), 1);
  const maxTimeFromData = d3Max(data, selectX);
  const minLastTime = maxTimeFromData > oneHour ? maxTimeFromData : oneHour;

  const xScale = d3ScaleTime()
    .domain([new Date(), minLastTime])
    .range([0, width]);

  const xAxis = d3AxisBottom()
    .scale(xScale)
    .ticks(d3TimeMinute.every(5));

  const xGridlines = d3AxisBottom()
    .tickFormat("")
    .tickSize(height)
    .ticks(d3TimeMinute.every(5))
    .scale(xScale);

  const yScaleDomain = d3Extent(data, selectY);

  const yScale = d3ScaleLinear()
    .domain([0, yScaleDomain[1]])
    .range([height - margin.bottom, 0]);

  const yAxis = d3AxisLeft()
    .scale(yScale)
    .ticks(yScaleDomain[1] / 5);

  const yGridlines = d3AxisLeft()
    .tickFormat("")
    .tickSize(-width)
    .scale(yScale);

  const selectScaledX = datum => xScale(selectX(datum));
  const selectScaledY = datum => yScale(selectY(datum));

  const circlePoints = data.map(datum => ({
    x: selectScaledX(datum),
    y: selectScaledY(datum)
  }));

  return (
    <svg className="container" height={height} width={width}>
      <g
        className="xAxis"
        key={`x-axis-${data.length}`}
        ref={node => d3Select(node).call(xAxis)}
        style={{
          transform: `translate(${margin.left}px, ${height - margin.bottom}px)`
        }}
      />
      <g
        className="gridlines"
        key={`x-gridlines-${data.length}`}
        ref={node => d3Select(node).call(xGridlines)}
        style={{ transform: `translate(${margin.left}px, 0)` }}
      />
      <g
        className="yAxis"
        key={`y-axis-${data.length}`}
        ref={node => d3Select(node).call(yAxis)}
        style={{ transform: `translate(${margin.left}px, 0)` }}
      />
      <g
        className="gridlines"
        key={`y-gridlines-${data.length}`}
        ref={node => d3Select(node).call(yGridlines)}
        style={{ transform: `translate(${margin.left}px, 0)` }}
      />
      <g
        className="scatter"
        style={{ transform: `translate(${margin.left}px, 0)` }}
      >
        {circlePoints.map(circlePoint => (
          <circle
            cx={circlePoint.x}
            cy={circlePoint.y}
            key={`${circlePoint.x},${circlePoint.y}`}
            r={4}
          />
        ))}
      </g>
    </svg>
  );
};

export default SurgeryTimeline;
