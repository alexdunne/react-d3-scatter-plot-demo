import React, { Component } from "react";
import addMinutes from "date-fns/add_minutes";

import SurgeryTimeline from "./components/SurgeryTimeline";
import "./App.css";

class App extends Component {
  interval = null;

  now = new Date();

  state = {
    data: [{ createdAt: new Date(), value: Math.random() * 100 }]
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      /// simulate 5 minutes passing
      this.now = addMinutes(this.now, 5);

      this.setState({
        data: [
          ...this.state.data,
          { createdAt: this.now, value: Math.random() * 100 }
        ]
      });
    }, 5000);
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  render() {
    return (
      <div className="App">
        <SurgeryTimeline
          data={this.state.data}
          height={500}
          width={1000}
          selectX={datum => datum.createdAt}
          selectY={datum => datum.value}
        />
      </div>
    );
  }
}

export default App;
